/*
 * MemCacheFree
 * Copyright (C) 2024 半透明狐人間 (半狐,TranslucentFoxHuman,TlFoxHuman)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.tlfoxhuman.memcachefree;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Process rtm = null;
        try {
            rtm = Runtime.getRuntime().exec("su -c echo 3 > /proc/sys/vm/drop_caches");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        int res = 0;
        String line ="" ;
        BufferedReader input = new BufferedReader(new InputStreamReader(rtm.getInputStream()));
        String returnstring = "";
        while (true) {
            try {
                if (!((line = input.readLine()) != null)) break;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            returnstring = returnstring + line + "\n";
        }



        try {
            res = rtm.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }



        if (res != 0) {
            if (returnstring == "" || returnstring == null){
                returnstring = "Unknown error. Maybe no Root privileges?";
            }
            Toast toast = Toast.makeText(
                    this, "Error: " + returnstring, Toast.LENGTH_SHORT);
            toast.show();
            Log.e("MemCacheFree",returnstring);

        }
        finish();
    }
}